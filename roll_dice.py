#!/usr/bin/env python3

"""This script is used to roll dice of arbitrary number and type.

Dice are given via command line with the notation: integer followed by a 'd' (for dice) or a 'w'
(for würfel) followed by an integer. Multiple dice can be passed.

Examples:
        1d4
        3w20
        534d825
"""

import argparse
import random
import re
from typing import List, Optional, Tuple


def parse_dice_type(dice_string: str) -> Optional[Tuple[int, int]]:
    """Parses a string for dice and returns the parsed dice

    The recognized dice notation is: integer followed by a 'd' (for dice) or a 'w' (for würfel)
    followed by an integer.

    Examples:
        1d4
        3w20
        534d825

    :param dice_string: The string that contains the dice notation
    :returns: A tuple containing the number of dice and the sides per die or None if the notation
        could not be parsed
    """
    regex = r'^(\d+)[wd](\d+)$'
    pattern = re.compile(regex)
    match = pattern.match(dice_string)

    if match:
        number = int(match.group(1))
        sides = int(match.group(2))
        return (number, sides)

    return None


def roll_dice(number: int, sides: int, exploding: bool = False) -> List[int]:
    """Calculates random numbers according to the given dice types

    Each die (given by the parameter 'numbers') is thrown with the number of sides given by the
    parameter 'sides'. The parameter 'exploding' indicates if a die should be rerolled if the
    first roll showed the highest roll possible, e. g. 20 for a die with 20 sides. Throws can
    explode infinitely; the end result of that die is the added number of each individual throw of
    that die.

    :param number: The number of dice to roll
    :param sides: The number of sides each die has
    :param exploding: Indicates if a die should be rerolled if the first roll showed the highest
        roll possible, e. g. 20 for a die with 20 sides
    :returns: A list of the end results of each die (i. e. the length of the return list is equal
        to the input parameter 'number')
    """
    throws = []

    for _ in range(number):
        exploded_throw = 0
        throw = random.randint(1, sides)
        exploded_throw += throw

        if exploding:
            while throw == sides:
                throw = random.randint(1, sides)
                exploded_throw += throw

        throws.append(exploded_throw)

    return throws


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument('dice', nargs='+',
                        help="The number of dice and sides to throw. Format: 2d6 or 2w6. Multiple "
                             "of these can be passed.")
    parser.add_argument('-x', '--exploding', action='store_true',
                        help="Enables exploding dice, i. e. if the highest possible number was "
                             "thrown for a die, the die will be thrown again until a number lower "
                             "than the maximum is thrown; all throws are then added for the final "
                             "result")

    args = parser.parse_args()

    for input_dice in args.dice:
        dice = parse_dice_type(input_dice)
        if dice:
            result = roll_dice(dice[0], dice[1], args.exploding)
            print(f"{input_dice}: {result} ({sum(result)})")
        else:
            print(f"{input_dice}: dice notation not recognized")
