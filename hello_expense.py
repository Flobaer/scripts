#!/usr/bin/env python3

import argparse
import csv
import datetime

from typing import Any, Dict, List, Optional, Set, Tuple

import matplotlib.pyplot as plt


class Ledger:
    def __init__(self, config: Dict[str, Any] = None):
        self.entry_list: List[Dict[str, Any]] = []

        if config:
            if config["file_name"]:
                header = ['date', 'category', 'amount', 'currency', 'comment',
                          'tags', 'unknown']

                with open(config["file_name"], 'r') as f:
                    csv_reader = csv.DictReader(f, fieldnames=header,
                                                restkey='rest')

                    for row in csv_reader:
                        entry: Dict[str, Any] = {}

                        entry['date'] = datetime.datetime.strptime(
                            row['date'], '%d/%m/%Y'
                        ).date()
                        entry['category'] = row['category']
                        entry['amount'] = int(row['amount'].replace('.', '')
                                              .replace(',', ''))
                        entry['currency'] = row['currency']
                        entry['comment'] = row['comment']
                        entry['tags'] = set(row['tags'].split(','))
                        entry['unknown'] = row['unknown']

                        self.entry_list.append(entry)

            if config["filter_rules"]:
                self.filter(**config["filter_rules"])

    def sum(self, entry_type: str = None) -> int:
        total = 0

        for entry in self.entry_list:
            if entry_type:
                if entry_type == 'expense' and entry['amount'] > 0:
                    total += entry['amount']
                elif entry_type == 'revenue' and entry['amount'] < 0:
                    total += entry['amount']
            else:
                total += entry['amount']

        return total

    def average(self) -> Dict[str, float]:
        if not self.entry_list:
            return {}

        total_sum: int = 0
        min_date = self.entry_list[0]['date']
        max_date = self.entry_list[len(self.entry_list) - 1]['date']

        for entry in self.entry_list:
            total_sum += entry['amount']

            min_date = min(min_date, entry['date'])
            max_date = max(max_date, entry['date'])

        years = (max_date.year - min_date.year) + 1
        days = (max_date - min_date).days
        months = 0

        if min_date.year == max_date.year:
            months = (max_date.month - min_date.month) + 1
        else:
            months += (12 - min_date.month) + 1
            months += max_date.month
            months += (max_date.year - min_date.year - 1) * 12

        return {'day': total_sum / days, 'month': total_sum / months,
                'year': total_sum / years}

    def filter(self, start_date: str = None,
               end_date: str = None,
               entry_type: str = None,
               categories: Set[str] = None,
               tags: Set[str] = None,
               comment: str = None) -> None:
        conditions = []

        if start_date:
            start = datetime.date.fromisoformat(start_date)
            conditions.append(lambda entry: entry['date'] >= start)

        if end_date:
            end = datetime.date.fromisoformat(end_date)
            conditions.append(lambda entry: entry['date'] <= end)

        if entry_type:
            if entry_type == 'expense':
                conditions.append(lambda entry: entry['amount'] > 0)
            elif entry_type == 'revenue':
                conditions.append(lambda entry: entry['amount'] < 0)

        if categories:
            conditions.append(lambda entry: entry['category'] in categories)

        if tags:
            conditions.append(lambda entry: entry['tags'].issubset(tags))

        if comment:
            conditions.append(
                lambda entry: comment.lower() in entry['comment'].lower()
            )

        if not conditions:
            return

        filtered_entries: List[Dict[str, Any]] = []

        for entry in self.entry_list:
            check_passed = True

            for condition in conditions:
                if not condition(entry):
                    check_passed = False
                    break

            if check_passed:
                filtered_entries.append(entry)

        self.entry_list = filtered_entries

    def group_by_category(self) -> Optional[Tuple[Dict[str, int],
                                                  datetime.date,
                                                  datetime.date]]:
        if not self.entry_list:
            return None

        grouping: Dict[str, int] = {}
        min_date = self.entry_list[0]['date']
        max_date = self.entry_list[len(self.entry_list) - 1]['date']

        for entry in self.entry_list:
            min_date = min(min_date, entry['date'])
            max_date = max(max_date, entry['date'])

            if entry['category'] not in grouping:
                grouping[entry['category']] = 0

            grouping[entry['category']] += entry['amount']

        return (grouping, min_date, max_date)

    def _autopct_labels(self, percentage: float, values, threshold=0.05):
        if percentage < threshold:
            return ""

        absolute = int(percentage * sum(values))

        return "{:.1f} %\n({:d} €)".format(percentage * 100, absolute)

    def _adjust_labels(self, labels, values, threshold=0.05):
        adjusted_labels = []

        total_value = sum(values)

        for (i, v) in enumerate(values):
            percentage = v / total_value
            if percentage < threshold:
                adjusted_label = "{} ({:.2f} %, {} €)".format(labels[i],
                                                              percentage * 100,
                                                              int(v))
                adjusted_labels.append(adjusted_label)

            else:
                adjusted_labels.append(labels[i])

        return adjusted_labels

    def pie_chart_by_category(self, normalize: str = '',
                              save_to_file: str = '') -> None:
        grouping_info = self.group_by_category()
        if grouping_info:
            grouping, start, end = grouping_info

        labels, values = zip(*sorted([(k, grouping[k]) for k in grouping],
                                     key=lambda t: t[1], reverse=True))

        title = "Expenses\nFrom: {}\nTo: {}".format(start, end)

        if normalize == 'month':
            title += "\nAverage per month"
            days = (end - start).days
            values = tuple(map(lambda v: (v / days * 7 * 4.33) / 100, values))
        else:
            values = tuple(map(lambda v: v / 100, values))

        plt.title(title)

        plt.pie(
            values, labels=self._adjust_labels(labels, values), startangle=180,
            autopct=lambda percentage: self._autopct_labels(percentage / 100,
                                                            values),
            counterclock=False, wedgeprops={'linewidth': 1,
                                            'edgecolor': "black"}
        )

        plt.annotate("Total: {:.2f} €".format(sum(values)),
                     (0, 0), (0, -20), xycoords='axes fraction',
                     textcoords='offset points', va='top')
        plt.axis('equal')

        if save_to_file:
            if isinstance(save_to_file, str):
                file_name = save_to_file
            else:
                date = datetime.datetime.now().strftime("%Y-%m-%dT%H-%M-%S")
                file_name = date + "_expenses_pie.png"

            plt.savefig(file_name, dpi=1000, format='png')
            plt.close()
        else:
            plt.show()

    def __str__(self):
        ledger_representation = []

        for entry in self.entry_list:
            entry_representation = []

            entry_representation.append(entry["date"].isoformat())
            entry_representation.append("{:13}".format(entry["category"]))
            entry_representation.append(
                "{:6.2f} {}".format(entry["amount"]/100, entry["currency"])
            )

            tags = ', '.join(entry["tags"])
            if tags.strip():
                entry_representation.append(tags)

            entry_representation.append(entry["comment"])

            ledger_representation.append(" | ".join(entry_representation))

        return "\n".join(ledger_representation)


def parse_args():
    parser = argparse.ArgumentParser()

    parser.add_argument(
        'input_file',
        help="File to be processed, i. e. CSV file that was exported from "
             "within the Hello Expense App"
    )
    parser.add_argument(
        '--start',
        help="Start date from when on entries are included. Format: YYY-MM-DD"
    )
    parser.add_argument(
        '--end',
        help="End date until entries are included. Format: YYY-MM-DD"
    )
    parser.add_argument(
        '--tags',
        help="Tags to consider. Separate multiple with comma."
    )
    parser.add_argument(
        '-t', '--type',
        help="Type of the entry, either 'expense' or 'revenue'"
    )

    args = parser.parse_args()

    config = {}
    config["filter_rules"] = {}

    config["file_name"] = args.input_file

    if args.start:
        config["filter_rules"]["start_date"] = args.start

    if args.end:
        config["filter_rules"]["end_date"] = args.end

    if args.type:
        config["filter_rules"]["entry_type"] = args.type

    if args.tags:
        config["filter_rules"]["tags"] = args.tags.split(",")

    return config


if __name__ == '__main__':
    config = parse_args()
    ledger = Ledger(config)
    ledger.pie_chart_by_category(normalize='month')
