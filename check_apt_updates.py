#!/usr/bin/env python3

"""This scripts checks if there are system package updates available and sends an email, if so.

Works for Debian based Linux distributions, i. e. depends on the package manager 'apt'
"""

import argparse
import configparser
import smtplib
import ssl
import subprocess


def send_mail(config: configparser.ConfigParser):
    server = config['email']['Server']
    password = config['email']['Password']
    port = int(config['email']['Port'])
    login_address = config['email']['LoginAddress']
    sender_address = config['email']['SenderAddress']
    receiver_address = config['email']['ReceiverAddress']

    message_template = (
        "Subject: [Heimdall] System package updates available\n\n"
        "There are system package updates available on Heimdall."
    )

    message = message_template.format()

    context = ssl.create_default_context()

    with smtplib.SMTP_SSL(server, port, context=context) as connection:
        connection.login(login_address, password)
        connection.sendmail(sender_address, receiver_address, message)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "-e", "--emailconfig", required=True,
        help="The file containing (sensitive) email configuration information"
    )

    args = parser.parse_args()

    email_config = configparser.ConfigParser()
    email_config.read(args.emailconfig)

    process = subprocess.Popen(["apt", "update"], stdout=subprocess.PIPE,
                               stderr=subprocess.DEVNULL)

    if process.stdout:
        for line in process.stdout.read().decode('utf-8').splitlines():
            if line == 'All packages are up to date.':
                break
        else:
            send_mail(email_config)
