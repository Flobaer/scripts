import argparse
import configparser
import ipaddress
import logging
import pathlib
import smtplib
import ssl
import sys

from selenium.webdriver import Firefox
from selenium.webdriver.firefox.options import Options
from selenium.common.exceptions import (NoSuchElementException, StaleElementReferenceException,
                                        WebDriverException)


LOG_FILE = '/home/pi/logs/hetzner_update.log'
GECKO_PATH = '/home/pi/scripts/geckodriver'


def send_mail(config: configparser.SectionProxy) -> None:
    server = config['Server']
    password = config['Password']
    port = int(config['Port'])
    login_address = config['LoginAddress']
    sender_address = config['SenderAddress']
    receiver_address = config['ReceiverAddress']

    message = (
        "Subject: Error in DNS update script\n\n"
        "Hetzner's DNS configuration page (DOM) seems to have changed.\n"
        "IP address(es) could not be updated."
    )

    context = ssl.create_default_context()

    with smtplib.SMTP_SSL(server, port, context=context) as connection:
        connection.login(login_address, password)
        connection.sendmail(sender_address, receiver_address, message)


def update_DNS_server(browser, ip_address: str, config: configparser.SectionProxy) -> bool:
    try:
        ipaddress.IPv4Address(ip_address)
    except ipaddress.AddressValueError:
        logging.exception("DNS update function was called with an invalid IPv4 address")
        return False

    LOGIN_GET_PAGE = 'https://konsoleh.your-server.de'
    DNS_EDIT_PAGE = 'https://konsoleh.your-server.de/dns.php'

    FORM_SELECTOR = "div.contentpart>form"
    SAVE_LINK_SELECTOR = "a.icon[title=Speichern]"

    # login to konsoleH
    browser.get(LOGIN_GET_PAGE)

    try:
        username_form = browser.find_element_by_id('login_user_inputbox')
    except NoSuchElementException:
        logging.exception("DOM element with id 'login_user_inputbox' not found")
        return False

    username_form.send_keys(config['UserName'])

    try:
        password_form = browser.find_element_by_id('login_pass_inputbox')
    except NoSuchElementException:
        logging.exception("DOM element with id 'login_pass_inputbox' not found")
        return False

    password_form.send_keys(config['Password'])

    try:
        browser.find_element_by_class_name('big_button').click()
    except NoSuchElementException:
        logging.exception("DOM element with class 'big_button' not found")
        return False

    browser.get(DNS_EDIT_PAGE)

    record_forms = browser.find_elements_by_css_selector(FORM_SELECTOR)
    if not record_forms:
        logging.error("No record forms found")
        return False

    # update www record
    for record_form in record_forms:
        input_fields = record_form.find_elements_by_css_selector('input')

        # check if form is that of www record
        www_record_found = False
        for input_field in input_fields:
            if input_field.get_attribute("value") == 'www':
                www_record_found = True
                break

        if www_record_found:
            for input_field in input_fields:
                value = input_field.get_attribute("value")
                try:
                    # check if the input field contains a valid IPv4 address as value, in which
                    # case the address will be updated to the new IP address
                    ipaddress.IPv4Address(value)
                    input_field.clear()
                    input_field.send_keys(ip_address)
                    try:
                        record_form.find_element_by_css_selector(SAVE_LINK_SELECTOR).click()
                    except NoSuchElementException:
                        logging.exception("DOM element for saving 'www' record not found ")
                        return False
                    break
                except ipaddress.AddressValueError:
                    pass
            else:
                logging.error("no input field with a valid IPv4 address for 'www' record found")

            break
    else:
        logging.error("no record with hostname value 'www' found")
        return False

    record_forms = browser.find_elements_by_css_selector(FORM_SELECTOR)
    if not record_forms:
        logging.error("No record forms found")
        return False

    # update @ record
    for record_form in record_forms:
        input_fields = record_form.find_elements_by_css_selector('input')

        # check if form is that of @ record
        at_record_found = False
        for input_field in input_fields:
            if input_field.get_attribute("value") == '@':
                at_record_found = True
                break

        if at_record_found:
            for input_field in input_fields:
                value = input_field.get_attribute("value")
                try:
                    # check if the input field contains a valid IPv4 address as value, in which
                    # case the address will be updated to the new IP address
                    ipaddress.IPv4Address(value)
                    input_field.clear()
                    input_field.send_keys(ip_address)
                    try:
                        record_form.find_element_by_css_selector(SAVE_LINK_SELECTOR).click()
                    except NoSuchElementException:
                        logging.exception("DOM element for saving '@' record not found ")
                        return False
                    break
                except ipaddress.AddressValueError:
                    pass
            else:
                logging.error("no input field with a valid IPv4 address for '@' record found")

            break
    else:
        logging.error("no record with hostname value '@' found")
        return False

    browser.close()

    return True


if __name__ == '__main__':
    log_dir = pathlib.Path(LOG_FILE).parent
    if not log_dir.exists():
        log_dir.mkdir(parents=True)

    logging.basicConfig(filename=LOG_FILE, filemode='a', level=logging.WARNING,
                        format='[%(asctime)s] %(levelname)s: %(message)s')

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--ipv4",
        help="The new (current) IPv4 that should replace the old IPv4 address "
             "on the DNS server (A record for 'www' and '@')"
    )

    parser.add_argument(
        "-s", "--secretconfig", required=True,
        help="The file containing (sensitive) email configuration information"
    )

    args = parser.parse_args()

    secret_config = configparser.ConfigParser()
    secret_config.read(args.secretconfig)

    options = Options()
    options.headless = True
    try:
        browser = Firefox(options=options, executable_path=GECKO_PATH)
    except WebDriverException as error:
        logging.exception(error)
        send_mail(secret_config['email'])
        sys.exit()

    try:
        update_successful = update_DNS_server(browser, args.ipv4, secret_config['hetzner'])
        if not update_successful:
            send_mail(secret_config['email'])

    except StaleElementReferenceException as error:
        logging.exception(error)
        send_mail(secret_config['email'])
    finally:
        browser.close()
