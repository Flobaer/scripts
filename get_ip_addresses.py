import argparse
import configparser
import csv
import datetime
from ipaddress import AddressValueError, IPv4Address, IPv6Address
import logging
import pathlib
import re
import smtplib
import subprocess
import ssl
from typing import Dict, Optional, Tuple, Union
import urllib.request


PYTHON = '/home/pi/.virtualenvs/hetzner_update_DNS/bin/python3'
DNS_UPDATE_SCRIPT = '/home/pi/scripts/hetzner_update_DNS.py'
LOG_FILE = '/home/pi/logs/get_ip_addresses.log'

# set up logging
logger = logging.getLogger(__name__)

log_handler = logging.FileHandler(LOG_FILE)
log_formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
log_handler.setFormatter(log_formatter)

logger.addHandler(log_handler)
logger.setLevel(logging.DEBUG)


def get_public_ipv4() -> Optional[IPv4Address]:
    command = 'dig +short -4 myip.opendns.com @resolver1.opendns.com'
    content = subprocess.check_output(command.split()).decode('utf-8').strip()

    if not content:
        logger.info("Public IPv4 could not be determined via opendns.com")
        request = urllib.request.urlopen('https://ipecho.net/plain')
        content = request.read().decode('utf-8')

    public_ipv4 = None

    try:
        public_ipv4 = IPv4Address(content)
    except AddressValueError:
        logger.exception("Public IPv4 address could not be determined")

    return public_ipv4


def get_public_ipv6() -> Optional[IPv6Address]:
    command = 'dig +short -6 myip.opendns.com AAAA @resolver1.opendns.com'
    content = subprocess.check_output(command.split()).decode('utf-8').strip()

    public_ipv6 = None

    try:
        public_ipv6 = IPv6Address(content)
    except AddressValueError:
        logger.exception("Public IPv6 address could not be determined")

    return public_ipv6


def get_private_ipv4() -> Optional[IPv4Address]:
    command = 'ip route get 1.1.1.1'
    content = subprocess.check_output(command.split()).decode('utf-8').strip()
    pattern = re.compile(r"src\s(\d+\.\d+\.\d+\.\d+)")
    match = pattern.search(content)

    private_ipv4 = None

    if match:
        try:
            private_ipv4 = IPv4Address(match.group(1))
        except AddressValueError:
            logger.exception("Private IPv4 address could not be determined")

    return private_ipv4


def get_private_ipv6() -> Optional[IPv6Address]:
    command = 'ip -6 route get 2606:4700:4700::1111'
    content = subprocess.check_output(command.split()).decode('utf-8').strip()
    pattern = re.compile(r"src\s(([0-9a-fA-F]*:*)*)")
    match = pattern.search(content)

    private_ipv6 = None

    if match:
        try:
            private_ipv6 = IPv6Address(match.group(1))
        except AddressValueError:
            logger.exception("Private IPv6 address could not be determined")

    return private_ipv6


def get_previous_ip_information(ip_file: pathlib.Path) -> Dict[Tuple[str, int],
                                                               Union[IPv4Address, IPv6Address]]:
    ip_info: Dict[Tuple[str, int], Union[IPv4Address, IPv6Address]] = {}

    if ip_file.is_file():
        with open(ip_file, 'r') as csv_file:
            reader = csv.DictReader(csv_file)
            entries = list(reader)
            if entries:
                last_entry = entries[-1]
                if not all(last_entry.values()):
                    logger.error(f"The last entry of IP file '{ip_file}' is not in the correct "
                                 f"CSV format: {last_entry}")
                else:
                    try:
                        ip_info[('public', 4)] = IPv4Address(last_entry['PublicIPv4'])
                        ip_info[('public', 6)] = IPv6Address(last_entry['PublicIPv6'])
                        ip_info[('private', 4)] = IPv4Address(last_entry['PrivateIPv4'])
                        ip_info[('private', 6)] = IPv6Address(last_entry['PrivateIPv6'])
                    except AddressValueError:
                        ip_info = {}
                        logger.exception(f"The last entry of IP file '{ip_file}' does not contain "
                                         f"valid IP addresses: {last_entry}")
            else:
                logger.warning(f"IP file {ip_file} does not contain any entries")

    return ip_info


def send_mail(old_ip, new_ip, config: configparser.SectionProxy) -> None:
    server = config['Server']
    password = config['Password']
    port = int(config['Port'])
    login_address = config['LoginAddress']
    sender_address = config['SenderAddress']
    receiver_address = config['ReceiverAddress']

    message_template = (
        "Subject: Heimdall's {0} IPv{1} address changed\n\n"
        "Heimdall's {0} IPv{1} address changed.\n\n"
        "Old address: {2}\n"
        "New address: {3}\n"
    )

    if old_ip.is_private:
        ip_type = "private"
    else:
        ip_type = "public"

    ip_version = old_ip.version

    message = message_template.format(ip_type, ip_version, old_ip, new_ip)

    context = ssl.create_default_context()

    with smtplib.SMTP_SSL(server, port, context=context) as connection:
        connection.login(login_address, password)
        connection.sendmail(sender_address, receiver_address, message)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    parser.add_argument(
        "-f", "--file", required=True, type=pathlib.Path,
        help="The CSV file where both the previous IP address information can "
             "be found and where the new IP address information will be "
             "appended to"
    )

    parser.add_argument(
        "-s", "--secretconfig", required=True, type=pathlib.Path,
        help="The file containing (sensitive) email configuration information"
    )

    args = parser.parse_args()

    secret_config = configparser.ConfigParser()
    secret_config.read(args.secretconfig)

    ip_file = pathlib.Path(__file__).parent / 'ip_address_info.csv'

    if args.file:
        logger.info(f"Using IP file provided via command line: {ip_file}")
        ip_file = args.file

    if not ip_file.exists():
        logger.info(f"IP file {ip_file} does not yet exist -> creating it")
        with open(ip_file, 'w') as f:
            f.write("Date,PublicIPv4,PublicIPv6,PrivateIPv4,PrivateIPv6\n")
    elif not ip_file.is_file():
        logger.error(f"IP file '{ip_file}' exists but is not a file")

    old_info = get_previous_ip_information(ip_file)
    new_info = {('public', 4): get_public_ipv4(),
                ('public', 6): get_public_ipv6(),
                ('private', 4): get_private_ipv4(),
                ('private', 6): get_private_ipv6()}

    update_csv_file = False
    if old_info:
        for key in new_info:
            if old_info[key] != new_info[key]:
                update_csv_file = True

                if key == ('public', 4):
                    logger.info(f"Public IPv4 has changed from {old_info[key]} to "
                                f"{new_info[key]}, sending email and updating DNS record(s)")
                    send_mail(old_info[key], new_info[key], secret_config['email'])
                    cli_args = ['--ipv4', str(new_info[('public', 4)]), '-s', args.secretconfig]
                    subprocess.call([PYTHON] + [DNS_UPDATE_SCRIPT] + cli_args)
    elif all(new_info.values()):
        logger.info("Old IP information could not be determined entirely but new info could")
        update_csv_file = True

    if update_csv_file:
        new_row: Dict[str, Union[str, None, IPv4Address, IPv6Address]] = {}
        new_row['Date'] = datetime.datetime.now().isoformat()
        new_row['PublicIPv4'] = new_info[('public', 4)]
        new_row['PublicIPv6'] = new_info[('public', 6)]
        new_row['PrivateIPv4'] = new_info[('private', 4)]
        new_row['PrivateIPv6'] = new_info[('private', 6)]

        if all(new_row.values()):
            logger.info("Updating IP file")
            with open(str(ip_file), 'a') as csv_file:
                writer = csv.DictWriter(
                    csv_file, lineterminator="\n",
                    fieldnames=['Date', 'PublicIPv4', 'PublicIPv6', 'PrivateIPv4', 'PrivateIPv6']
                )
                writer.writerow(new_row)
