from http.server import BaseHTTPRequestHandler, HTTPServer
import logging
from logging import handlers
import lzma
import os
import pathlib
import ssl
import subprocess


HOST = ''
PORT = 54321
INTERFACE = 'eth0'  # local interface from which so send Magic Packet
MAC = '50:e5:49:d1:10:c6'  # remote MAC address of device to wake up
home_dir = pathlib.Path(os.path.expanduser("~"))
ssl_dir = pathlib.Path('/etc/letsencrypt/live/fwerner.eu')
CERTFILE = ssl_dir / 'fullchain.pem'
KEYFILE = ssl_dir / 'privkey.pem'
log_file = pathlib.Path('/var/log/custom_logs/wake_on_lan/wake_on_lan.log')


class WolHTTPRequestHandler(BaseHTTPRequestHandler):
    def do_GET(self):
        self.send_response(200)
        self.end_headers()

        wol_command = '/usr/sbin/etherwake -i {} {}'.format(INTERFACE, MAC)
        return_code = subprocess.call(wol_command.split(' '))
        if return_code == 0:
            logger.info("Magic packet sent to {}".format(MAC))
            self.wfile.write(b'Successfully sent Magic Packet for Wake-on-LAN')
        else:
            logger.info("Error: sending of Magic Packet to {} was NOT "
                        "successful".format(MAC))
            self.wfile.write(
                b'Error: sending of Magic Packet was NOT successful'
            )

    def log_message(self, format, *args):
        logger.info("{} - - [{}] {}".format(self.client_address[0],
                                            self.log_date_time_string(),
                                            format % args))


class TimedRotatingFileHandler(handlers.TimedRotatingFileHandler):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)

    def doRollover(self):
        super().doRollover()
        log_dir = pathlib.Path(self.baseFilename).parent
        files_to_compress = [f for f in log_dir.iterdir() if f.is_file()
                             and f.suffix not in ['.log', '.xz']]
        for file_to_compress in files_to_compress:
            data = b''
            with open(str(file_to_compress), 'rb') as f:
                data = f.read()

            compressed_file = file_to_compress.with_suffix(
                file_to_compress.suffix + '.xz')
            with lzma.open(str(compressed_file), 'w') as f:
                f.write(data)

            file_to_compress.unlink()


if __name__ == '__main__':
    log_dir = log_file.parent
    if not log_dir.exists():
        log_dir.mkdir(parents=True)

    logger = logging.getLogger(__name__)
    logger.setLevel(logging.INFO)
    handler = TimedRotatingFileHandler(str(log_file), 'midnight')
    handler.setFormatter(logging.Formatter(
        "%(asctime)s: %(message)s",
        datefmt="%Y-%m-%dT%H:%M:%S")
    )
    logger.addHandler(handler)

    httpd = HTTPServer((HOST, PORT), WolHTTPRequestHandler)
    httpd.socket = ssl.wrap_socket(
        httpd.socket,
        certfile=str(CERTFILE),
        keyfile=str(KEYFILE),
    )
    httpd.serve_forever()
