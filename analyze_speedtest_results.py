#!/usr/bin/env python3

import argparse
import csv
from collections import namedtuple
import datetime
import platform
import sys
import textwrap


class Color:
    GREEN = '\033[92m' if platform.system() != "Windows" else ""
    RED = '\033[91m' if platform.system() != "Windows" else ""
    CLEAR = '\033[0m' if platform.system() != "Windows" else ""

def validate_thresholds(thresholds: str):
    threshold_min, threshold_norm, threshold_max = thresholds.split(",")
    return (float(threshold_min), float(threshold_norm), float(threshold_max))

def print_info(thresholds_download, thresholds_upload, from_date, to_date):
    print("Indicated internet speed thresholds:")
    print(f"  Minimum download speed: {thresholds_download.min} Mb/s")
    print(f"  Normal download speed:  {thresholds_download.norm} Mb/s")
    print(f"  Maximum download speed: {thresholds_download.max} Mb/s\n")
    print(f"  Minimum upload speed: {thresholds_upload.min} Mb/s")
    print(f"  Normal upload speed:  {thresholds_upload.norm} Mb/s")
    print(f"  Maximum upload speed: {thresholds_upload.max} Mb/s\n")

    from_date_string = "first measurement" if from_date is None else from_date
    to_date_string = "last measurement" if to_date is None else to_date
    print(f"Taking measurements into account from {from_date_string} to {to_date_string}\n")

    print("Performing the following tests:")
    print(
        "  Test 1: at least 90 % of the maximum download/upload speed "
        f"({thresholds_download.max} Mb/s / {thresholds_upload.max} Mb/s) must be achieved at "
        "least once"
    )
    print(
        "  Test 2: at least 90 % of the measurements must show the normal downlad/upload speed "
        f"({thresholds_download.norm} Mb/s / {thresholds_upload.norm} Mb/s)"
    )
    print(
        "  Test 3: every measurement must show at least the minimum download/upload speed "
        f"({thresholds_download.min} Mb/s / {thresholds_upload.min} Mb/s)\n\n"
    )

def main(input_file, output_file, from_date, to_date, thresholds_download, thresholds_upload):
    speedtests = csv.DictReader(input_file)

    max_down = -1
    max_up = -1

    min_down = float(2**16)
    min_up = float(2**16)

    cumulative_up = 0.0
    cumulative_down = 0.0
    no_of_measurements = 0

    test1_down_failed = 0
    test2_down_passed = 0
    test3_down_failed = 0

    test1_up_failed = 0
    test2_up_passed = 0
    test3_up_failed = 0

    if to_date is None:  # 0 x
        if from_date is None:  # 0 0
            def in_range(x): return True
        else:  # 0 1
            def in_range(x): return x >= from_date

    elif from_date is None:  # 1 0
        def in_range(x): return x <= to_date
    else:  # 1 1
        def in_range(x): return from_date <= x <= to_date

    for speedtest in speedtests:
        # timestamp from speedtest tool output has 'Z' as suffix for some reason
        test_time = datetime.datetime.fromisoformat(speedtest['Timestamp'][:-1])

        if in_range(test_time):
            down = float(speedtest['Download']) / (1024**2)
            up = float(speedtest['Upload']) / (1024**2)

            max_down = max(max_down, down)
            max_up = max(max_up, up)
            min_down = min(min_down, down)
            min_up = min(min_up, up)

            cumulative_up += up
            cumulative_down += down
            no_of_measurements += 1

            if down < 0.9 * thresholds_download.max:
                test1_down_failed += 1
            if down >= thresholds_download.norm:
                test2_down_passed += 1
            if down < thresholds_download.min:
                test3_down_failed += 1

            if up < 0.9 * thresholds_upload.max:
                test1_up_failed += 1
            if up >= thresholds_upload.norm:
                test2_up_passed += 1
            if up < thresholds_upload.min:
                test3_up_failed += 1

    test1_down = test2_down = test3_down = f"{Color.GREEN}passed{Color.CLEAR}"
    test1_up = test2_up = test3_up = f"{Color.GREEN}passed{Color.CLEAR}"

    if test1_down_failed == no_of_measurements:
        test1_down = f"{Color.RED}failed{Color.CLEAR}"
    if test2_down_passed < 0.9 * no_of_measurements:
        test2_down = f"{Color.RED}failed{Color.CLEAR}"
    if test3_down_failed > 0:
        test3_down = f"{Color.RED}failed{Color.CLEAR}"

    if test1_up_failed == no_of_measurements:
        test1_up = f"{Color.RED}failed{Color.CLEAR}"
    if test2_up_passed < 0.9 * no_of_measurements:
        test2_up = f"{Color.RED}failed{Color.CLEAR}"
    if test3_up_failed > 0:
        test3_up = f"{Color.RED}failed{Color.CLEAR}"

    output_file.write(f"Number of speed measurements: {no_of_measurements}\n")

    output_file.write(
        f"Test 1 (download): {test1_down} ({test1_down_failed} failed measurements)\n"
    )
    output_file.write(
        f"Test 2 (download): {test2_down} ({test2_down_passed / no_of_measurements * 100:.2f} % "
        f"of 90 %, i. e. {no_of_measurements - test2_down_passed} failed measurements out of "
        f"{no_of_measurements})\n"
    )
    output_file.write(f"Test 3 (download): {test3_down} ({test3_down_failed} failures)\n")

    output_file.write(f"Test 1 (upload): {test1_up} ({test1_up_failed} failed measurements)\n")
    output_file.write(
        f"Test 2 (upload): {test2_up} ({test2_up_passed / no_of_measurements * 100:.2f} % "
        f"of 90 %, i. e. {no_of_measurements - test2_up_passed} failed measurements out of "
        f"{no_of_measurements})\n"
    )
    output_file.write(f"Test 3 (upload): {test3_up} ({test3_up_failed} failures)\n\n")

    output_file.write(f"Maximum download speed: {max_down:.2f} Mb/s\n")
    output_file.write(f"Average download speed: {cumulative_down / no_of_measurements:.2f} Mb/s\n")
    output_file.write(f"Minimum download speed: {min_down:.2f} Mb/s\n\n")

    output_file.write(f"Maximum upload speed: {max_up:.2f} Mb/s\n")
    output_file.write(f"Average upload speed: {cumulative_up / no_of_measurements:.2f} Mb/s\n")
    output_file.write(f"Minimum upload speed: {min_up:.2f} Mb/s\n")


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        formatter_class=argparse.RawDescriptionHelpFormatter,
        epilog=textwrap.dedent("""\
            Example date arguments:
              --fd 2020-11-01 (every measurement from 1 November 2020 (including) up to now)
              --td 2020-11-30 (every measurement from now up to 1 November 2020 (including))
              --fd 2020-11-01T15:30 (every measurement from 1 November 2020 3:30 PM (including) up to now)
            """)
    )

    parser.add_argument(
        'input_file', type=argparse.FileType('r'),
        help="specify file containing speedtest results in tsv" "format"
    )

    parser.add_argument(
        '-o', '--output_file',
        type=argparse.FileType('w'), nargs='?', default=sys.stdout,
        help="specify output file; defaults to stdout"
    )

    parser.add_argument(
        '--from_date', '--fd', type=datetime.datetime.fromisoformat,
        help="specify earliest date and time from when speed tests will be taken into account; "
        "expected format is ISO 8601, i. e YYYY-MM-DDThh:mm:ss; smaller units can be omitted"
    )

    parser.add_argument(
        '--to_date', '--td', type=datetime.datetime.fromisoformat,
        help="specify last date and time up to which speed tests will be taken into account; "
        "expected format is ISO 8601, i. e. YYYY-MM-DDThh:mm:ss; smaller units can be omitted"
    )

    parser.add_argument(
        '--download_thresholds', '--ds', type=validate_thresholds,
        help="specify the expected (maximum) download speed in Mb/s; defaults to 25 (Mb/s)"
    )

    parser.add_argument(
        '--upload_thresholds', '--us', type=validate_thresholds,
        help="specify the expected (maximum) upload speed in Mb/s; defaults to 1 (Mb/s)"
    )

    args = parser.parse_args()

    Thresholds = namedtuple('Thresholds', 'min norm max')

    # O2 my Home L in Mb/s
    thresholds_download = Thresholds(50.0, 83.0, 100.0)
    thresholds_upload = Thresholds(10.0, 33.0, 40.0)

    if args.download_thresholds:
        thresholds_download = Thresholds(
            args.download_thresholds[0], args.download_thresholds[1], args.download_thresholds[2]
        )

    if args.upload_thresholds:
        thresholds_upload = Thresholds(
            args.upload_thresholds[0], args.upload_thresholds[1], args.upload_thresholds[2]
        )

    to_date = args.to_date

    if args.to_date is not None and args.to_date.time() == datetime.time.min:
        to_date = args.to_date.replace(hour=23, minute=59, second=59)

    print_info(thresholds_download, thresholds_upload, args.from_date, to_date)

    main(
        args.input_file, args.output_file, args.from_date, to_date, thresholds_download,
        thresholds_upload
    )
