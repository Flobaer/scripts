#!/usr/bin/env python3

import csv


def filter_contacts(input_file_name):
    contacts = []
    emails_seen = set()
    with open(input_file_name, 'r', encoding='latin-1') as input_csv_file:
        reader = csv.DictReader(input_csv_file)
        for row in reader:
            emails = []
            if row['E-Mail-Adresse']:
                emails.append(row['E-Mail-Adresse'])
            if row['E-Mail 2: Adresse']:
                emails.append(row['E-Mail 2: Adresse'])
            if row['E-Mail 3: Adresse']:
                emails.append(row['E-Mail 3: Adresse'])
            emails = {a for a in emails if a not in emails_seen}

            if len(emails) > 0:
                emails_seen.update(emails)
                emails = list(emails) + [''] * (3 - len(emails))
                contacts.append({
                    'Vorname': row['Vorname'],
                    'Nachname': row['Nachname'],
                    'E-Mail-Adresse': emails[0],
                    'E-Mail 2: Adresse': emails[1],
                    'E-Mail 3: Adresse': emails[2],
                })

    return contacts


def remove_duplicates(contact_lists):
    contacts = []
    emails_seen = set()

    for contact_list in contact_lists:
        for contact in contact_list:
            emails = [contact['E-Mail-Adresse'],
                      contact['E-Mail 2: Adresse'],
                      contact['E-Mail 3: Adresse']]
            emails = [email for email in emails if email != '']
            emails = [email for email in emails if email not in emails_seen]
            if len(emails) > 0:
                emails_seen.update(set(emails))
                emails = emails + [''] * (3 - len(emails))
                contact['E-Mail-Adresse'] = emails[0]
                contact['E-Mail 2: Adresse'] = emails[1]
                contact['E-Mail 3: Adresse'] = emails[2]
                contacts.append(contact)

    return contacts


def write_contacts_to_csv(contacts, file_prefix):
    chunk_size = 80
    contact_chunks = [
        contacts[i * chunk_size:(i + 1) * chunk_size]
        for i in range((len(contacts) + chunk_size - 1) // chunk_size)
    ]
    for i, chunk in enumerate(contact_chunks):
        file_name = "{}_{}.csv".format(file_prefix, i + 1)
        with open(file_name, 'w') as output_file:
            writer = csv.DictWriter(
                output_file,
                ['Vorname', 'Nachname', 'E-Mail-Adresse',
                 'E-Mail 2: Adresse', 'E-Mail 3: Adresse']
            )
            writer.writeheader()

            for contact in chunk:
                writer.writerow(contact)


if __name__ == '__main__':
    contact_lists = []
    contact_lists.append(filter_contacts('tmp/landshut-umlands.CSV'))
    contact_lists.append(filter_contacts('tmp/landshut.CSV'))
    contact_lists.append(filter_contacts('tmp/münchen.CSV'))

    normalized_contacts = remove_duplicates(contact_lists)
    write_contacts_to_csv(normalized_contacts, 'Galerie')
