#!/bin/bash

set -e

BACKUP_DIR="/storage/syncthing_backups"
ORIGINAL_DIR="/storage/Syncthing"
CURRENT_DATE=$(date +%s)
LOGFILE="$BACKUP_DIR/logs/${CURRENT_DATE}.log"
FOLDERS="
Camera_OnePlus3
Documents
Pictures
Projects
"

SECONDS_IN_A_WEEK=604800
SECONDS_IN_A_MONTH=2592000
SECONDS_IN_A_YEAR=31536000

exec 1>"$LOGFILE"
exec 2>&1

echo "Starting at:"
echo "$(date -d @${CURRENT_DATE} --iso-8601=seconds)"
echo ""

# annual backup
echo "--- Annual Backup Start ---"
time_delta=0

# check if no yearly backup exists yet
if [ -f "${BACKUP_DIR}/yearly/last_update.txt" ]; then
	last_update=$(cat "${BACKUP_DIR}/yearly/last_update.txt")
	last_update=$(date -d ${last_update} +%s || echo "")
	if [ -n "$last_update" ]; then
		time_delta=$(( $CURRENT_DATE - $last_update ))
	fi
fi

echo "time_delta: $time_delta"
echo "threshold : $SECONDS_IN_A_YEAR"
# if no yearly backup exists or yearly backup is more than a year ago
if [ $time_delta -eq 0 ] || [ $time_delta -gt ${SECONDS_IN_A_YEAR} ]; then
	echo "condition met: backup necessary"

	time_spec_found=""

	# determine backup chronologically closest to yearly
	for time_spec in daily weekly monthly; do
		if [ -f "${BACKUP_DIR}/${time_spec}/last_update.txt" ]; then
			time_spec_found=$(date -d $(cat "${BACKUP_DIR}/${time_spec}/last_update.txt") +%s || echo "")
		fi
	done

	# if time_spec_found not empty: rsync that backup to yearly
	if [ -n "$time_spec_found" ]; then
		echo "closest existing backup found in: $time_spec"
		src_folder="${BACKUP_DIR}/${time_spec}"

	# else rsync original syncthing folders to yearly
	else
		echo "no existing backup found for synchronization, defaulting to original"
		src_folder="${ORIGINAL_DIR}"
	fi

	echo "source folder for backup: $src_folder"

	for folder in $FOLDERS; do
		echo "synchronizing $src_folder/$folder to $BACKUP_DIR/yearly"
		rsync -ah --exclude="**/.stversions" "${src_folder}/${folder}" "${BACKUP_DIR}/yearly"
	done

	# update last_update.txt
	echo "$(date -d @${CURRENT_DATE} --iso-8601=seconds)" > "${BACKUP_DIR}/yearly/last_update.txt"
fi

echo "--- Annual Backup End ---"
echo ""

# monthly backup
echo "--- Monthly Backup Start ---"
time_delta=0

# check if no monthly backup exists yet
if [ -f "${BACKUP_DIR}/monthly/last_update.txt" ]; then
	last_update=$(cat "${BACKUP_DIR}/monthly/last_update.txt")
	last_update=$(date -d ${last_update} +%s || echo "")
	if [ -n "$last_update" ]; then
		time_delta=$(( $CURRENT_DATE - $last_update ))
	fi
fi

echo "time_delta: $time_delta"
echo "threshold : $SECONDS_IN_A_MONTH"
# if no monthly backup exists or monthly backup is more than a month ago
if [ $time_delta -eq 0 ] || [ $time_delta -gt ${SECONDS_IN_A_MONTH} ]; then
	echo "condition met: backup necessary"

	time_spec_found=""

	# determine backup chronologically closest to monthly
	for time_spec in daily weekly; do
		if [ -f "${BACKUP_DIR}/${time_spec}/last_update.txt" ]; then
			time_spec_found=$(date -d $(cat "${BACKUP_DIR}/${time_spec}/last_update.txt") +%s || echo "")
		fi
	done

	# if time_spec_found not empty: rsync that backup to monthly
	if [ -n "$time_spec_found" ]; then
		echo "closest existing backup found in: $time_spec"
		src_folder="${BACKUP_DIR}/${time_spec}"

	# else rsync original syncthing folders to monthly
	else
		echo "no existing backup found for synchronization, defaulting to original"
		src_folder="${ORIGINAL_DIR}"
	fi

	echo "source folder for backup: $src_folder"

	for folder in $FOLDERS; do
		echo "synchronizing $src_folder/$folder to $BACKUP_DIR/monthly"
		rsync -ah --exclude="**/.stversions" "${src_folder}/${folder}" "${BACKUP_DIR}/monthly"
	done

	# update last_update.txt
	echo "$(date -d @${CURRENT_DATE} --iso-8601=seconds)" > "${BACKUP_DIR}/monthly/last_update.txt"
fi

echo "--- Monthly Backup End ---"
echo ""

# weekly backup
echo "--- Weekly Backup Start ---"
time_delta=0

# check if no weekly backup exists yet
if [ -f "${BACKUP_DIR}/weekly/last_update.txt" ]; then
	last_update=$(cat "${BACKUP_DIR}/weekly/last_update.txt")
	last_update=$(date -d ${last_update} +%s || echo "")
	if [ -n "$last_update" ]; then
		time_delta=$(( $CURRENT_DATE - $last_update ))
	fi
fi

echo "time_delta: $time_delta"
echo "threshold : $SECONDS_IN_A_WEEK"
# if no weekly backup exists or weekly backup is more than a week ago
if [ $time_delta -eq 0 ] || [ $time_delta -gt ${SECONDS_IN_A_WEEK} ]; then
	echo "condition met: backup necessary"

	time_spec_found=""

	# determine backup chronologically closest to weekly
	for time_spec in daily; do
		if [ -f "${BACKUP_DIR}/${time_spec}/last_update.txt" ]; then
			time_spec_found=$(date -d $(cat "${BACKUP_DIR}/${time_spec}/last_update.txt") +%s || echo "")
		fi
	done

	# if time_spec_found not empty: rsync that backup to weekly
	if [ -n "$time_spec_found" ]; then
		echo "closest existing backup found in: $time_spec"
		src_folder="${BACKUP_DIR}/${time_spec}"

	# else rsync original syncthing folders to weekly
	else
		echo "no existing backup found for synchronization, defaulting to original"
		src_folder="${ORIGINAL_DIR}"
	fi

	echo "source folder for backup: $src_folder"

	for folder in $FOLDERS; do
		echo "synchronizing $src_folder/$folder to $BACKUP_DIR/weekly"
		rsync -ah --exclude="**/.stversions" "${src_folder}/${folder}" "${BACKUP_DIR}/weekly"
	done

	# update last_update.txt
	echo "$(date -d @${CURRENT_DATE} --iso-8601=seconds)" > "${BACKUP_DIR}/weekly/last_update.txt"
fi

echo "--- Weekly Backup End ---"
echo ""

# Daily backup
echo "--- Daily Backup Start ---"

src_folder="${ORIGINAL_DIR}"

echo "source folder for backup: $src_folder"

for folder in $FOLDERS; do
	echo "synchronizing $src_folder/$folder to $BACKUP_DIR/daily"
	rsync -ah --exclude="**/.stversions" "${src_folder}/${folder}" "${BACKUP_DIR}/daily"
done

# update last_update.txt
echo "$(date -d @${CURRENT_DATE} --iso-8601=seconds)" > "${BACKUP_DIR}/daily/last_update.txt"

echo "--- Daily Backup End ---"  # debug

echo ""
echo "Finished by:"
echo "$(date --iso-8601=seconds)"

# compress log file
xz "$LOGFILE"
