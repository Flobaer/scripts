import datetime as dt
from enum import Enum
import gzip
import os
from pathlib import Path
import re
import subprocess
import sys
from typing import Dict, Tuple


MINECRAFT_DIR = Path('/home/john/minecraft_1.19')
WORLD_NAME = 'tocors_world'
BACKUP_DIR = Path('/home/john/backups/minecraft_worlds')


class Status(Enum):
    ONLINE = 1
    OFFLINE = 2


def get_player_status(log_file: str, player_status: Dict[str, Tuple[Status, dt.datetime]] = None) -> Dict[str, Tuple[Status, dt.datetime]]:
    if not player_status:
        player_status = {}

    if os.path.basename(log_file) == 'latest.log':
        log_file_date = dt.date.today()
        log_file_date = dt.datetime.fromtimestamp(
            os.path.getmtime(log_file)).date()
    else:
        file_name = os.path.basename(log_file)
        try:
            log_file_date = dt.datetime.strptime(file_name[:10],
                                                 "%Y-%m-%d").date()
        except ValueError:
            print("Error: file names are expected to either contain an "
                  "ISO 8601 date at the beginning or be exactly named "
                  "'latest.log'")
            print("Erroneous file name: {}".format(log_file))
            sys.exit()

    player_regex = r"""
    \[(\d\d:\d\d:\d\d)\]
    .*
    \s(.*)\s
    (joined|left)
    \sthe\sgame
    """

    player_pattern = re.compile(player_regex, re.VERBOSE)

    if log_file.endswith('.gz'):
        with gzip.open(log_file) as f:
            log = f.read().decode('utf-8').splitlines()
    else:
        with open(log_file, 'r') as f:
            log = f.read().splitlines()

    for line in log:
        match = player_pattern.match(line)
        if match:
            player = match.group(2)
            log_time = dt.datetime.strptime(match.group(1),
                                            "%H:%M:%S").time()
            log_date = dt.datetime.combine(log_file_date, log_time)

            if match.group(3) == 'joined':
                status = Status.ONLINE
            elif match.group(3) == 'left':
                status = Status.OFFLINE

            if (player in player_status
                    and player_status[player][1] < log_date):
                player_status[player] = (status, log_date)
            elif player not in player_status:
                player_status[player] = (status, log_date)

    return player_status


def process_log_files(directory: Path) -> Dict[str, Tuple[Status, dt.datetime]]:
    log_files = [os.path.join(directory, f) for f in os.listdir(directory)]
    player_status: Dict[str, Tuple[Status, dt.datetime]] = {}

    for log_file in log_files:
        player_status = get_player_status(log_file, player_status)

    return player_status


def shutdown_condition_player(player_status: Dict[str, Tuple[Status, dt.datetime]]) -> bool:
    time_last_player_left = dt.datetime.min

    for player in player_status:
        if player_status[player][0] == Status.ONLINE:
            return False

        if time_last_player_left < player_status[player][1]:
            time_last_player_left = player_status[player][1]

    time_delta = dt.datetime.now() - time_last_player_left
    time_delta_in_minutes = time_delta.days * 24 * 60 + time_delta.seconds / 30

    if time_delta_in_minutes > 15:
        return True

    else:
        return False


def backup_world(world_folder: Path, backup_folder: Path):
    timestamp = dt.datetime.now().isoformat().replace(':', '-')
    backup_name = timestamp + '_' + world_folder.name + '.tar.gz'

    subprocess.run(['tar', 'czf', backup_folder / backup_name, world_folder])


def system_uptime():
    with open('/proc/uptime', 'r') as f:
        uptime_seconds = float(f.readline().split()[0])
        return uptime_seconds / 60


if __name__ == '__main__':
    player_status = process_log_files(MINECRAFT_DIR / 'logs')

    if (shutdown_condition_player(player_status)
            and system_uptime() > 15
            and not subprocess.check_output("who")):

        backup_world(MINECRAFT_DIR / WORLD_NAME, BACKUP_DIR)
        os.system("shutdown -h now")
