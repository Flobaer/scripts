#!/usr/bin/env python3

import argparse
import datetime
import pathlib
import os
import shutil
import sys

SAVE_FILES = {
    "ds1": "/media/Adanos/SteamLibrary/steamapps/compatdata/211420/pfx/drive_c/users/steamuser/My Documents/NBGI/DarkSouls/DRAKS0005.sl2",
    "dsr": "/media/Adanos/SteamLibrary/steamapps/compatdata/570940/pfx/drive_c/users/steamuser/My Documents/NBGI/DARK SOULS REMASTERED/79972162/DRAKS0005.sl2"
}

BACKUP_DIRS = {
    "ds1": "/media/Adanos/Backups/game_saves/Dark_Souls_Prepare_to_Die",
    "dsr": "/media/Adanos/Backups/game_saves/Dark_Souls_Remastered"
}

# DS1_SAVE_FILE = "/media/Adanos/SteamLibrary/steamapps/compatdata/211420/pfx/drive_c/users/steamuser/My Documents/NBGI/DarkSouls/DRAKS0005.sl2"
# DS1_BACKUP_DIR = "/media/Adanos/Backups/game_saves/Dark_Souls"
# 
# DSR_SAVE_FILE = "/media/Adanos/SteamLibrary/steamapps/compatdata/570940/pfx/drive_c/users/steamuser/My Documents/NBGI/DARK SOULS REMASTERED/79972162/DRAKS0005.sl2"
# DSR_BACKUP_DIR = "/media/Adanos/Backups/game_saves/Dark_Souls_Remastered"

def back_up_current_save_file(save_file, backup_dir):
    timestamp = datetime.datetime.now().strftime("%Y-%m-%dT%H-%M-%S")
    backup_dir = pathlib.Path(backup_dir) / timestamp
    save_file = pathlib.Path(save_file)

    try:
        print(f"Creating backup directory '{backup_dir}'")
        os.mkdir(backup_dir)
    except FileExistsError as error:
        print(f"{error}")
        sys.exit()

    print(f"Copying:\n{save_file}")
    copied_file = shutil.copy2(save_file, backup_dir)
    print(f"Copied file to:\n{copied_file}")

def restore_latest_save_file(save_file, backup_dir):
    latest_backup_dir = ""
    latest_backup_time = datetime.datetime.min
    backups = os.listdir(backup_dir)

    for directory in backups:
        try:
            # expected format: YYYY-MM-DDTHH-MM-SS
            #           e. g.: 2020-12-17T12-06-56
            # however for datetime.isoformat time must be separated with colons, not hyphens
            backup_date, backup_time = directory.split("T")
            backup_time = backup_time.replace("-", ":")
            backup_iso_format = "T".join([backup_date, backup_time])
            backup_datetime_object = datetime.datetime.fromisoformat(backup_iso_format)

            if backup_datetime_object > latest_backup_time:
                latest_backup_dir = directory
                latest_backup_time = backup_datetime_object

        except Exception:
            pass

    if latest_backup_dir:
        latest_backup_file = pathlib.Path(backup_dir) / latest_backup_dir / "DRAKS0005.sl2"
        save_file = pathlib.Path(save_file)

        print(f"Restoring backup save file\n{latest_backup_file}")
        print(f"to\n{save_file}")
        shutil.copyfile(latest_backup_file, save_file)


def parse_cli_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument("game", choices=["ds1", "dsr"],
                        help="ds1 = 'Dark Souls: Prepare to Die', dsr = 'Dark Souls: Remastered'")
    parser.add_argument("-r", "--restore", action="store_true",
                        help="Restore most recently backed up save file")

    return parser.parse_args()


if __name__ == '__main__':
    args = parse_cli_arguments()
    save_file = SAVE_FILES[args.game]
    backup_dir = BACKUP_DIRS[args.game]

    if args.restore:
        restore_latest_save_file(save_file, backup_dir)

    else:
        back_up_current_save_file(save_file, backup_dir)
